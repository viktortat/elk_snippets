# Elk Snippets

usage ./ek_helper_script_opendistro.sh "ek_index_show_size" - run ek_index_show_size

usage ./ek_helper_script_opendistro.sh "copy" - run ek_replica_set

usage ./ek_helper_script_opendistro.sh "ek_index_delete_wildcard" "docker_postgres"- delete wildcard index 

usage ./ek_helper_script_opendistro.sh "docker_truncate_logs" - truncate docker logs
