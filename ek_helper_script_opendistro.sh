#!/bin/bash
# usage ./ek_helper_script_opendistro.sh "ek_index_show_size" - run ek_index_show_size
# usage ./ek_helper_script_opendistro.sh "copy" - run ek_replica_set
# usage ./ek_helper_script_opendistro.sh "ek_index_delete_wildcard" "docker_postgres"- delete wildcard index 
# usage ./ek_helper_script_opendistro.sh "docker_truncate_logs" - truncate docker logs
action="${1}"
index_name="${2}"
ek_key_store="/docker-compose/SWARM/opendistro-elasticsearch"
curator_config_dir="/docker-compose/SWARM/curator/configs/curator"
helper_filename="ek_helper_script_opendistro.sh"

precheck(){
echo "action is ${action}"


if [[ "${action}" == "" ]];then
	echo "arg1 is empty"
	echo "example usage"
	echo "Usage /docker-compose/SWARM/opendistro-elasticsearch/ek_helper_script_opendistro.sh \"ek_index_show_size\""
	echo "Usage /docker-compose/SWARM/opendistro-elasticsearch/ek_helper_script_opendistro.sh \"copy\""
	exit 1
fi


docker_elk_master=$(docker ps | grep elas | grep master | awk '{ print $1 }')

if [[ "${docker_elk_master}" == "" ]];then
	echo "docker elk master not found"
	exit 1
else
	echo "Docker ek master ok"
fi
}


ek_copy_script(){

	docker cp ${ek_key_store}/ek_helper_script_opendistro.sh ${docker_elk_master}:/usr/share/elasticsearch/data/
	docker exec -i ${docker_elk_master} bash -c "bash /usr/share/elasticsearch/data/ek_helper_script_opendistro.sh \"run\""

}


ek_replica_set(){
echo "exec set \"number_of_replicas\" : 0"
curl  --insecure --cert "/usr/share/elasticsearch/config/kirk.pem" --key "/usr/share/elasticsearch/config/kirk-key.pem" -H "Content-Type: application/json" -XPUT 'https://127.0.0.1:9200/_settings'  --user "admin:admin"  -d '{
    "index" : {
        "number_of_replicas" : 0
    }
}'

}

ek_index_delete_wildcard(){
local arg_index_name="${1}"
echo "delete index ${index_name}"
docker exec -i ${docker_elk_master} bash -c "curl -k --cert /usr/share/elasticsearch/config/kirk.pem --key /usr/share/elasticsearch/config/kirk-key.pem -XDELETE -H \"Content-Type: application/json\" \"https://127.0.0.1:9200/${arg_index_name}-*\"   "
}

ek_index_show_size(){
	echo "exec function ${FUNCNAME[0]}"
	index_list=$(docker exec -i ${docker_elk_master} bash -c "curl -k --cert /usr/share/elasticsearch/config/kirk.pem --key /usr/share/elasticsearch/config/kirk-key.pem \"https://127.0.0.1:9200/_cat/indices?v\"   ")
	echo "${index_list}" | awk '{ print $3 " " $9 }' | sort -r -k2 -n
}


ek_copy_keys(){

if [[ ! -d "${ek_key_store}" ]];then
mkdir -p ${ek_key_store}
fi

docker cp $(docker ps | grep elas | grep mas | awk '{print $NF}'):/usr/share/elasticsearch/config/kirk-key.pem ${ek_key_store}/keys/kirk-key.pem
docker cp $(docker ps | grep elas | grep mas | awk '{print $NF}'):/usr/share/elasticsearch/config/kirk.pem ${ek_key_store}/keys/kirk.pem
}

ek_copy_key_curator(){
if [[ ! -d "${curator_config_dir}" ]];then
mkdir -p ${curator_config_dir}
fi

docker cp $(docker ps | grep elas | grep mas | awk '{print $NF}'):/usr/share/elasticsearch/config/root-ca.pem ${curator_config_dir}/root-ca.pem
}

docker_truncate_logs(){
local cur_user=$(whoami)
local get_docker_system_dir=$(docker info  | grep 'Root Dir' | awk '{print $NF}')
local exec_prefix=""
if [[ "${cur_user}" != "root" ]];then
	local exec_prefix="sudo"
fi

echo "Current user ${cur_user}. exec_prefix is ${exec_prefix}"
if [[ "${get_docker_system_dir}" == "" ]];then
		echo "docker dir invalid. Current value is ${get_docker_system_dir} "
		exit 1
else
		echo "Truncate *.log files in ${get_docker_system_dir}/containers"
		sleep 7;
		${exec_prefix} find ${get_docker_system_dir}/containers  -type f -name '*-json.log'  -print0  | ${exec_prefix}  xargs -0 truncate  -s 0
		echo "Truncating done"
fi
}


main(){
	
	if [[ "${action}" = "docker_truncate_logs" ]];then
	docker_truncate_logs
	exit 0
	fi
	precheck
	ek_copy_keys
	ek_copy_key_curator
	if [[ "${action}" = "run" ]];then
	ek_replica_set
	fi
	if [[ "${action}" = "copy" ]];then
	ek_copy_script
	fi
	if [[ "${action}" = "ek_index_show_size" ]];then
		ek_index_show_size
	fi
	if [[ "${action}" = "ek_index_delete_wildcard" ]] && [[ "${index_name}" != "" ]];then
		ek_index_delete_wildcard "${index_name}"
	fi

}

main